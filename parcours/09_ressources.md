
# Ressources

## Références en markdown/bibtex

Plusieurs cas de figure :

1. Si vous souhaitez citer l'auteur + l'année et la page entre parenthèses :

Affichage dans Atom | Affichage définitif
:--|:--
`L’espace réel, celui de notre vie matérielle, et le cyberespace (qui n’est certes pas si complètement virtuel) ne devraient pas faire l’objet d’appellations séparées puisqu’ils s’interpénètrent de plus en plus fermement [@shirky_here_2008, p. 194].` | `L’espace réel, celui de notre vie matérielle, et le cyberespace (qui n’est certes pas si complètement virtuel) ne devraient pas faire l’objet d’appellations séparées puisqu’ils s’interpénètrent de plus en plus fermement (Shirky 2008, 194).`

2. Si le nom de l'auteur apparaît déjà, et que vous souhaitez simplement ajouter l'année de publication entre parenthèses :

Affichage dans Atom | Affichage définitif
:--|:--
`Clay @shirky_here_2008[p. 194] a suggéré que l’espace réel, celui de notre vie matérielle, et le cyberespace (qui n’est certes pas si complètement virtuel) ne devraient pas faire l’objet d’appellations séparées puisqu’ils s’interpénètrent de plus en plus fermement.` | `Clay Shirky (2008, 194), a suggéré que l’espace réel, celui de notre vie matérielle, et le cyberespace (qui n’est certes pas si complètement virtuel) ne devraient pas faire l’objet d’appellations séparées puisqu’ils s’interpénètrent de plus en plus fermement.`

3. Afin d'éviter la répétition d'un nom, et indiquer seulement l'année, faire figurer un `-` devant la clé.


Affichage dans Atom | Affichage définitif
:--|:--
`Des artistes conceptuels avaient cherché (apparemment sans grand succès ou sans grande conviction si l’on en croit Lucy Lippard [-@lippard_six_1973 ; -@lippard_get_1984]) à contourner les règles du marché de l’art.` | `Des artistes conceptuels avaient cherché (apparemment sans grand succès ou sans grande conviction si l’on en croit Lucy Lippard (1973 ; 1984)) à contourner les règles du marché de l’art.`
