
# Édition avec Pandoc

## Prise en main de Pandoc

Pandoc permet de convertir des documents numériques en ligne de commande via un terminal.

La commande `pandoc` appelle un ou plusieurs fichiers en entrée ainsi que leur format d'entrée, un format de sortie ou de conversion, et le fichier de sortie.

Par exemple pour convertir un fichier au format Markdown en HTML: `pandoc -f md -t html fichier.md -o fichier.html`

La commande `pandoc` est hautement configurable. Il faut consulter la [documentation](https://pandoc.org/MANUAL.html) régulièrement en fonction de vos besoins d'éditeur.

Pour convertir d'un format à l'autre, Pandoc utilise des _templates_ ou des modèles de documents. Il est possible de personnaliser ces modèles, comme nous allons le voir dans la suite de l'atelier.

Quelques options de la commande Pandoc à connaître :

- `--standalone` (produit un document complet incluant les métadonnées)
- `--bibliography=references.bib` (spécifie un fichier de références bibliographiques)
- `--template=mytemplate.html` (spécifie un modèle)
- `--toc` (table of contents)
- `--help` (résume les options existantes)

Quelques utilisations possibles :

- vous souhaitez récupérer le contenu d'un document Word (.docx) et poursuivre son édition en Markdown :

      pandoc -f docx -t markdown --extract-media=./ --atx-headers fichier.docx -o fichier.md

- vous souhaitez convertir en HTML un document en Markdown comprenant des références bibliographiques (references.bib) :

      pandoc --standalone --ascii --bibliography=references.bib -f markdown -t html fichier.md -o fichier.html


## Exercice de conversion

- Export simple: (génère du html correctement formé, mais un document incomplet)

      pandoc -f markdown -t html monfichier.md -o export.html

- Export complet "standalone": (génère un document html complet)

      pandoc --standalone -f markdown -t html monfichier.md -o export.html

- Ajout des métadonnées en en-tête de fichier. Vous pouvez ajouter les lignes suivantes en entête de fichier markdown pour associer à votre document les principales métadonnées.

```yaml
---
title:
subtitle:
date:
author:
- Aristotle
- Peter Abelard
abstract:
keywords:
bibliography:
lang:
---
```

Ces métadonnées sont celles que Pandoc utilise par défaut pour produire les documents dans les différents formats. Mais selon les formats (il en existe d'autres) il est possible de créer ses propres métadonnées et de les utiliser en modifiant les modèles par défaut de Pandoc.

Ces métadonnées peuvent soit être placées en tête de fichier Markdown, soit être placées dans un fichier séparé, au format YAML (extension : `.yaml`). Dans ce cas, la commande Pandoc précédente devra préciser où sont les métadonnées (ici dans un fichier `metadonnes.yaml`) :

      pandoc --standalone -f markdown -t html monfichier.md metadonnees.yaml -o export.html

- Export en prenant en compte le fichier de bibliographie avec l'option `--bibliography=FILE`:

      pandoc --standalone --bibliography=monfichier.bib --filter pandoc-citeproc -f markdown -t html monfichier.md -o export.html

  - Vous pouvez aussi déclarer le fichier de référence une fois pour toute dans l'en-tête yaml :

```yaml
---
bibliography: monfichier.bib
---
```

  et lancer la commande précédente (sans l'option `---bibliography=monfichier.bib`)

      pandoc --standalone --filter pandoc-citeproc -f markdown -t html monfichier.md -o export.html

- Pour transformer les appels de références en hyperliens, ajouter une ligne dans l'en-tête yaml :

```yaml
---
link-citations: true
---
```

- Ajout d'une table des matières avec l'option `--toc`:

      pandoc --standalone --toc  --filter pandoc-citeproc -f markdown -t latex monfichier.md -o export.pdf

- Export en XML TEI avec l'option `-t tei`:

      pandoc --standalone  --filter pandoc-citeproc -f markdown -t tei monfichier.md -o export.xml

- Conversion d'un fichier ODT (docx fonctionne aussi) en markdown, ce qui produit une meilleure base de travail (remarque : l'option `--extract-media` permet d'extraire les médias du document dans un sous-dossier `/media`)

      pandoc -f odt -t markdown --extract-media=./ --atx-headers export.odt -o exportMD.md

## Les templates dans Pandoc

Pandoc utilise des templates par défaut pour chacun des formats de sorties. Le principe du template est simple: Pandoc remplace les variables du template par les valeurs déclarées dans les métadonnées présentes soit dans un fichier yaml, soit dans l'entête yaml du fichier markdown.

Soit :

Yaml | template | Html (par exemple)
:--|:--|:--
`auteur: Niels Planel`  | `<meta name="author" content="$auteur$">`   |  `<meta name="author" content="Niels Planel">`

Pandoc ne nous permet pas réellement de toucher au corps du texte, à moins d'utiliser des options (voir [la documentation](https://pandoc.org/MANUAL.html)) ou des filtres particuliers que l'on peut soit-même créer, selon ses besoins.

Le corps du texte (c'est-à-dire l'intégralité du fichier markdown par exemple) est ajouté au document de sortie avec la variable `$body$`. S'il n'est pas possible d'intervenir dans le template de ce corps de texte, il est par contre possible d'intervenir avant ou après la variable `$body$` (pour ajouter du contenu générique par exemple), et d'intervenir sur les métadonnées du document.

Pour créer un template personnalisé, commençons par récupérer le template par défaut d'un format (ici HTML) dans un fichier `montemplate.html`.

      pandoc -D html > montemplate.html

Note : cette commande fonctionne pour tous les formats de sortie (sauf docx/odt qui sont gérés autrement, voir la documentation Pandoc).

Vous pouvez maintenant ouvrir le fichier `montemplate.html` dans Atom pour voir comment il est structuré.

Comme exercice nous allons ajouter une donnée dans le yaml, modifier le template en fonction et déclarer dans la commande Pandoc la prise en compte du nouveau template.

Dans les métadonnées yaml, nous allons ajouter la clé/valeur en ajoutant une simple ligne :

```yaml
image: auteur.jpg
```

Puis dans le template, nous pouvons ajouter une balise `<img>` :

`$if(image)$
<img src="$image$" width="30%"/>
$endif$
`

Enfin, pour que Pandoc prenne en compte le nouveau template, vous devez ajouter l'option `--template=FILE`, soit la commande complète suivante :

      pandoc --standalone --filter pandoc-citeproc --template=montemplate.html -f markdown -t html monfichier.md metadata.yaml -o export_2.html
