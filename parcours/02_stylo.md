# Exercice 1 : Stylo

## Présentation de l'outil

Stylo est un éditeur de texte simplifiant la rédaction et l'édition de documents scientifiques en sciences humaines et sociales.

Vous pouvez consulter [la documentation](http://stylo-doc.ecrituresnumeriques.ca/) ainsi qu'[une présentation vidéo](https://www.youtube.com/watch?v=qcwEqbcxBF8).

### Chaîne de publication

![Chaîne de publication Stylo](./media/chaine_stylo_sp.png)

## Fonctionnalités

L'environnement de travail de Stylo intègre une chaîne éditoriale complète basée sur [Pandoc](http://pandoc.org/) et outillée des modules suivants :

  - **un éditeur de métadonnées**
  - **un versionnage**
  - **une gestion de la bibliographie**
  - **différents formats exports** : HTML5, XML (TEI, Érudit), PDF...
  - **l'annotation**
  - **le partage de document**

### Édition

Un article dans Stylo est composé de trois éléments distincts :

  - un corps de texte
  - des métadonnées
  - une bibliographie structurée

![Stylo_3elements](./media/stylo_3f_marqued.png)

L'environnement d'édition est composé de 5 modules :

1. au centre : l'espace d'écriture, consacré au corps de texte de l'article
2. à droite : le bouton [Metadata] ouvre l'éditeur de métadonnées
3. à gauche :
  1. les Versions pour naviguer et agir sur les différentes versions de l'article
  2. le Sommaire de l'article liste les titres de niveau 2 et 3
  3. la Bibliographie liste les références bibliographiques
  4. les Statistiques offrent quelques données quantitatives sur l'article

![Stylo_Modules](./media/stylo_fullopen_marqued.png)


## Exercice

### Prise en main

Après vous être créé un compte sur [stylo.ecrituresnumeriques.ca](http://stylo.ecrituresnumeriques.ca), connectez-vous à Stylo. La première interface liste les documents que vous [n']avez [pas encore] créé.

Dans la liste des articles, ouvrez et consultez le document **How-to** qui vous permettra de découvrir les fonctionnalités de Stylo, et vous donnera l'essentiel de la syntaxe Markdown pour éditer un document.

À découvrir :

- le module de versionnage
- le sommaire
- la gestion des références
- les métadonnées
- la comparaison de versions

À éditer :

- déclarer un titre et niveaux de titre
- ajouter un italique
- ajouter une citation
- ajouter une image
- ajouter une note de bas de page
- ajouter une référence

Vous pouvez à tout moment visualiser un rendu du document en cours d'édition avec le bouton **[Annotate]**.

### Edition d'un article

L'exercice va consister à éditer un article simple à l'aide de Stylo dans le format Markdown.

- Article source : «Stratégie anti-pauvreté» de Niels Planel [sur Sens Public](http://sens-public.org/article1359.html).

**Consignes :**

1. créer un nouveau document dans Stylo
2. reporter le texte dans Stylo
3. éditer le texte en Markdown et traiter notamment :
    - les hyperliens: `[texte du lien](http://lien.ca)`
    - les italiques: `un mot _en italique_`
    - les espaces insécables: en ASCII `&nbsp;`, ou sur Stylo `CTRL+ESPACE`
    - les citations: avec `> ` en début de ligne
4. à titre pédagogique, ajouter des éléments supplémentaires :
    - 2 niveaux de titre: `#` niveau 1 et `##` niveau 2
    - une ou plusieurs note.s de bas de page: `[^note]` pour l'appel de note dans le texte et `[^note]: Ma note.` pour le texte de la note en fin de document
    - une image: `![Légende de l'image](http://lien.ca/vers/image.jpg)`
5. traiter les deux références bibliographiques:
    - générer les références en bibtex (voir tutoriel [Comment importer rapidement une bibliographie vers Zotéro ?](http://www.youtube.com/watch?v=S-f3J9LOqdQ))
    - intégrer les clés BibTeX dans l'article (voir [Références en Markdown/BibTeX](./parcours/09_ressources.md#r%C3%A9f%C3%A9rences-en-markdownbibtex))

Les deux références utilisés en bibtex :

```bibtex
@book{castel1995metamorphoses,
  title={Les m{\'e}tamorphoses de la question sociale},
  author={Castel, Robert},
  year={1995},
  publisher={Paris, Fayard}
}
```

```bibtex
@article{hinton2016war,
  title={From the War on Poverty to the War on Crime: The Making of Mass Incarceration in America},
  author={Hinton, Elizabeth},
  year={2016},
  publisher={Harvard University Press, Cambridge, MA}
}
```

## Exports de Stylo

Stylo permet de produire plusieurs formats à partir du format pivot en Markdown.

Il est possible de choisir entre deux styles de citation : ![moduleexport](./media/exportmodal.png)

1. _inline citations_ : la référence de type _(Goody, 1976)_ est ajoutée dans le corps du texte,
2. _footnotes citations_ : la référence est intégrée avec un appel de note.

|bouton|fonction|
|:-:|:--|
|![[preview]](uploads/images/preview.png) | pour ouvrir l'aperçu html de l'article   |
|![[HTML]](uploads/images/html.png) | pour télécharger une version html prête à mettre en ligne |
|![[XML (erudit])](uploads/images/xml.png) | pour prévisualiser une version xml au schéma Erudit |
|![[ZIP]](uploads/images/zip.png) | pour télécharger les trois composants de l'article : métadonnées (.yaml), bibliographie (.bib), corps de texte (.md)  |

Au prochain exercice, nous allons utiliser les sources du `.zip` pour personnaliser la production de document.

---
Voir la suite [[04_edition](./04_edition.md)]
